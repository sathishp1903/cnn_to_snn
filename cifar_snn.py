import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import hdf5storage as io
import pickle
from keras.utils import to_categorical

# CL and PL Definition
def conv2d(x, W, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    return x

def maxpool2d(x, k=2):
    return tf.nn.avg_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='SAME')

# Loading the testing set
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
test_x = np.divide(x_test[:],255.)
test_y = to_categorical(y_test)

# Constants
num_examples = 1000
num_batches = 10
weights = {}
total_accuracy =[]
dt = 0.0001
duration = 0.045
max_rate = 10000
vt = 1

# Batches
batch_x = test_x.reshape(num_batches,num_examples,32,32,3)
batch_y = test_y.reshape(num_batches,num_examples,10)

# Loading the weights
weights['wc1'] = io.loadmat('cifar_weights/an_wc1.mat')['wc1']
weights['wc2'] = io.loadmat('cifar_weights/an_wc2.mat')['wc2']
weights['wc3'] = io.loadmat('cifar_weights/an_wc3.mat')['wc3']
weights['wd1'] = io.loadmat('cifar_weights/an_wd1.mat')['wd1']
weights['out'] = io.loadmat('cifar_weights/an_out.mat')['out']

# Membrane Potentials
x = np.zeros([num_examples,32,32,3])
vm1 = np.zeros([num_examples,32,32,64])
vm2 = np.zeros([num_examples,16,16,64])
vm3 = np.zeros([num_examples,16,16,128])
vm4 = np.zeros([num_examples,8,8,128])
vm5 = np.zeros([num_examples,8,8,256])
vm6 = np.zeros([num_examples,4,4,256])
vm7 = np.zeros([num_examples,1024])
vm8 = np.zeros([num_examples,10])

# Spiking out at each layer
s1 = np.zeros([num_examples,32,32,64])
s2 = np.zeros([num_examples,16,16,64])
s3 = np.zeros([num_examples,16,16,128])
s4 = np.zeros([num_examples,8,8,128])
s5 = np.zeros([num_examples,8,8,256])
s6 = np.zeros([num_examples,4,4,256])
s7 = np.zeros([num_examples,1024])
s8 = np.zeros([num_examples,10])

# Spiking at output layer
final_out = np.zeros([num_examples,10])

# Place Holders
cl1_in = tf.placeholder("float", [num_examples,32,32,3])
pl1_in = tf.placeholder("float", [num_examples,32,32,64])
cl2_in = tf.placeholder("float", [num_examples,16,16,64])
pl2_in = tf.placeholder("float", [num_examples,16,16,128])
cl3_in = tf.placeholder("float", [num_examples,8,8,128])
pl3_in = tf.placeholder("float", [num_examples,8,8,256])
fc1_in = tf.placeholder("float", [num_examples,4,4,256])

# Network Definition
conv1 = conv2d(cl1_in, weights['wc1'])
conv2 = maxpool2d(pl1_in, k=2)
conv3 = conv2d(cl2_in, weights['wc2'])
conv4 = maxpool2d(pl2_in, k=2)
conv5 = conv2d(cl3_in, weights['wc3'])
conv6 = maxpool2d(pl3_in, k=2)
fulcon1 = tf.reshape(fc1_in, [-1, 4096])
fulcon1 = tf.matmul(fulcon1, weights['wd1'])

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for itr in range(num_batches):
    	for i in np.arange(dt,duration,dt):
    		rescale_fac = 1/(dt*max_rate);
    		spike_snapshot = np.random.uniform(0,1,[num_examples,32,32,3]) * rescale_fac;
    		x = (batch_x[itr]>=spike_snapshot).astype(float)
    		cv1 = sess.run(conv1,feed_dict={cl1_in:x})
        	vm1 = vm1 + cv1
        	s1[vm1>vt] = 1 
        	vm1[vm1>vt] = 0
        	cl1_out = s1	
        	cv2 = sess.run(conv2,feed_dict={pl1_in: cl1_out})
        	cl1_out[cl1_out>0] = 0
        	vm2 = vm2 + cv2
        	s2[vm2>vt] = 1 
        	vm2[vm2>vt] = 0
        	pl1_out = s2
        	cv3 = sess.run(conv3,feed_dict={cl2_in: pl1_out})
        	pl1_out[pl1_out>0] = 0
        	vm3 = vm3 + cv3
        	s3[vm3>vt] = 1 
        	vm3[vm3>vt] = 0
        	cl2_out = s3
        	cv4 = sess.run(conv4,feed_dict={pl2_in: cl2_out})
        	cl2_out[cl2_out>0] = 0
        	vm4 = vm4 + cv4
        	s4[vm4>vt] = 1 
        	vm4[vm4>vt] = 0
        	pl2_out = s4
        	cv5 = sess.run(conv5,feed_dict={cl3_in: pl2_out})
        	pl2_out[pl2_out>0] = 0
        	vm5 = vm5 + cv5
        	s5[vm5>vt] = 1 
        	vm5[vm5>vt] = 0
        	cl3_out = s5
        	cv6 = sess.run(conv6,feed_dict={pl3_in: cl3_out})
        	cl3_out[cl3_out>0] = 0
        	vm6 = vm6 + cv6
        	s6[vm6>vt] = 1 
        	vm6[vm6>vt] = 0
        	pl3_out = s6
        	fc1 = sess.run(fulcon1,feed_dict={fc1_in: pl3_out})
        	pl3_out[pl3_out>0] = 0
        	vm7 = vm7 + fc1
        	s7[vm7>vt] = 1
        	vm7[vm7>vt] = 0
        	fc1_out = s7
        	out = np.matmul(fc1_out, weights['out'])
        	fc1_out[fc1_out>0] = 0
        	vm8 = vm8 + out
        	s8[vm8>vt] = 1
        	vm8[vm8>vt] = 0
        	final_out += s8
        	s8[s8>0] = 0
        	x[x>0] = 0
        vm1 = np.zeros([num_examples,32,32,64])
	vm2 = np.zeros([num_examples,16,16,64])
	vm3 = np.zeros([num_examples,16,16,128])
	vm4 = np.zeros([num_examples,8,8,128])
	vm5 = np.zeros([num_examples,8,8,256])
	vm6 = np.zeros([num_examples,4,4,256])
	vm7 = np.zeros([num_examples,1024])
	vm8 = np.zeros([num_examples,10])
	test_out = np.zeros((num_examples))
	label = np.zeros((num_examples))
	correct = np.zeros((num_examples))
	test_out = np.argmax(final_out, axis = 1)
	label = np.argmax(batch_y[itr], axis = 1)
	correct = (test_out == label).astype(float)
	total_correct = correct.sum()
	accuracy = ((total_correct) / float(num_examples)) * 100
	print "Accuracy of batch ", itr, " is " + str(accuracy)
        final_out[final_out>0] = 0
	total_accuracy.append(accuracy)
final_accuracy = sum(total_accuracy)/len(total_accuracy)
print "Final Accuracy is",final_accuracy
