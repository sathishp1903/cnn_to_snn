import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import hdf5storage as io

#CL and PL definition
def conv2d(x, W, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='VALID')
    return x

def maxpool2d(x, k=2):
    return tf.nn.avg_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='VALID')

# Read the testing data
data = input_data.read_data_sets('data/',one_hot=True)
test_x = data.test.images.reshape(-1,28,28,1)
test_y = data.test.labels

# Load the normalized weights obtained by training using CNN
weights = {}
weights['wc1'] = io.loadmat('mnist_weights/an_wc1.mat')['wc1']
weights['wc2'] = io.loadmat('mnist_weights/an_wc2.mat')['wc2']
weights['wd1'] = io.loadmat('mnist_weights/an_wd1.mat')['wd1']
weights['out'] = io.loadmat('mnist_weights/out.mat')['out']

# Memberane Potential
x = np.zeros([10000,28,28,1])
vm1 = np.zeros([10000,24,24,16])
vm2 = np.zeros([10000,12,12,16])
vm3 = np.zeros([10000,8,8,16])
vm4 = np.zeros([10000,4,4,16])
vm5 = np.zeros([10000,256])
vm6 = np.zeros([10000,10])

# Spikes at each layer
s1 = np.zeros([10000,24,24,16])
s2 = np.zeros([10000,12,12,16])
s3 = np.zeros([10000,8,8,16])
s4 = np.zeros([10000,4,4,16])
s5 = np.zeros([10000,256])
s6 = np.zeros([10000,10])

# Final Layer Spike output
final_out = np.zeros([10000,10])

# Place Holders
cl1_in = tf.placeholder("float", [10000,28,28,1])
pl1_in = tf.placeholder("float", [10000,24,24,16])
cl2_in = tf.placeholder("float", [10000,12,12,16])
pl2_in = tf.placeholder("float", [10000, 8, 8,16])

# Network CL and PL
conv1 = conv2d(cl1_in, weights['wc1'])
conv2 = maxpool2d(pl1_in, k=2)
conv3 = conv2d(cl2_in, weights['wc2'])
conv4 = maxpool2d(pl2_in, k=2)

# Threshold voltage
vt = 1

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init) 
    for i in np.arange(0.001,0.055,0.001):
        rescale_fac = 1/(0.001*1000);
        spike_snapshot = np.random.uniform(0,1,[10000,28,28,1]) * rescale_fac;
        x = (test_x>=spike_snapshot).astype(float)
	    #print "x is",np.count_nonzero(x)
        cv1 = sess.run(conv1,feed_dict={cl1_in:x})
        x[x>0] = 0
        vm1 = vm1 + cv1
        s1[vm1>vt] = 1 
        vm1[vm1>vt] = 0
        cl1_out = s1	
        #print "s1 is ",np.count_nonzero(s1)    
        cv2 = sess.run(conv2,feed_dict={pl1_in: cl1_out})
        cl1_out[cl1_out>0] = 0
        vm2 = vm2 + cv2
        s2[vm2>vt] = 1 
        vm2[vm2>vt] = 0
        pl1_out = s2
        #print "s2 is ",np.count_nonzero(s2)
        cv3 = sess.run(conv3,feed_dict={cl2_in: pl1_out})
        pl1_out[pl1_out>0] = 0
        vm3 = vm3 + cv3
        s3[vm3>vt] = 1 
        vm3[vm3>vt] = 0
        cl2_out = s3
        #print "s3 is ",np.count_nonzero(s3)
        cv4 = sess.run(conv4,feed_dict={pl2_in: cl2_out})
        cl2_out[cl2_out>0] = 0
        vm4 = vm4 + cv4
        s4[vm4>vt] = 1 
        vm4[vm4>vt] = 0
        pl2_out = s4
        #print "s4 is ",np.count_nonzero(s4)
        fc1 = np.reshape(pl2_out, [-1, 256])
        fc1 = np.matmul(fc1, weights['wd1'])
 	    #print "fc1 is ",np.count_nonzero(fc1)
        pl2_out[pl2_out>0] = 0
        vm5 = vm5 + fc1
        s5[vm5>vt] = 1
        vm5[vm5>vt] = 0
        fc1_out = s5
        #print "s5 is ",np.count_nonzero(s5)
        out = np.matmul(fc1_out, weights['out'])
        fc1_out[fc1_out>0] = 0
        vm6 = vm6 + out
        s6[vm6>vt] = 1
        vm6[vm6>vt] = 0
        final_out += s6
        #print "s6 is ",np.count_nonzero(s6)
        #print "final_out is ",np.count_nonzero(final_out)
        s6[s6>0] = 0
 
test_out = np.zeros((10000))
label = np.zeros((10000))
correct = np.zeros((10000))
test_out = np.argmax(final_out, axis = 1)
label = np.argmax(test_y, axis = 1)
correct = (test_out == label).astype(float)
total_correct = correct.sum()

accuracy = ((total_correct) / float(10000)) * 100
print "Accuracy is " + str(accuracy)
