CNN to SNN Conversion for Image Classification in MNIST and CIFAR-10 datasets

The examples are implemented using TensorFlow, where the training takes place using CNN
and then converted to SNN.

Files Included
==============

> MNIST
a. Training and Testing using CNN (mnist_cnn.py)
b. CNN TO SNN conversion and testing using SNN (mnist_snn.py)

> CIFAR-10
a. Testing using CNN (cifar_cnn.py)
b. Conersion of CNN to SNN and testing with SNN (cifar_snn.py)

Drive Links
===========

> MNIST Training Weights
https://drive.google.com/drive/folders/1VI13WEtkN8yKxemchjuXn52j_IIkANVY?usp=sharing

> CIFAR-10 Training Weights
https://drive.google.com/drive/folders/1Owveb-EXV5lpwYGBfJtqPdbIoicmBNXQ?usp=sharing