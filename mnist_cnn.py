import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import hdf5storage as io

# Read the input data and reshape
data = input_data.read_data_sets('data/',one_hot=True)
train_x = data.train.images.reshape(-1, 28, 28, 1)
test_x = data.test.images.reshape(-1,28,28,1)
train_y = data.train.labels
test_y = data.test.labels

# Constants
training_iters = 100 
learning_rate = 0.001 
batch_size = 128

# MNIST data input (img shape: 28*28)
n_input = 28

# MNIST total classes (0-9 digits)
n_classes = 10

# Placeholders
x = tf.placeholder("float", [None, 28,28,1])
y = tf.placeholder("float", [None, n_classes])


# Function for Convolution and Pooling Layers
def conv2d(x, W, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='VALID')
    return tf.nn.relu(x) 

def maxpool2d(x, k=2):
    return tf.nn.avg_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='VALID')

# Weight Matrix
weights = {
    'wc1': tf.get_variable('W0', shape=(5,5,1,16), initializer=tf.contrib.layers.xavier_initializer()), 
    'wc2': tf.get_variable('W1', shape=(5,5,16,16), initializer=tf.contrib.layers.xavier_initializer()), 
    'wd1': tf.get_variable('W2', shape=(4*4*16,256), initializer=tf.contrib.layers.xavier_initializer()), 
    'out': tf.get_variable('W3', shape=(256,n_classes), initializer=tf.contrib.layers.xavier_initializer()), 
}

# Network Definition
# CL and PL
conv1 = conv2d(x, weights['wc1'])
conv2 = maxpool2d(conv1, k=2)
conv3 = conv2d(conv2, weights['wc2'])
conv4 = maxpool2d(conv3, k=2)

# Fully connected layer
fc1 = tf.reshape(conv4, [-1, weights['wd1'].get_shape().as_list()[0]])
fc1 = tf.matmul(fc1, weights['wd1'])
fc1 = tf.nn.relu(fc1) 
out = tf.matmul(fc1, weights['out'])

# Cost and Accuracy Evaluation
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=out, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
correct_prediction = tf.equal(tf.argmax(out, 1), tf.argmax(y, 1)) 
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init) 
    train_loss = []
    test_loss = []
    train_accuracy = []
    test_accuracy = []
    print "Train_x len is"
    print len(train_x)
    print "train_y len is"
    print len(train_y)
    for i in range(training_iters):
        for batch in range(len(train_x)//batch_size):
            batch_x = train_x[batch*batch_size:min((batch+1)*batch_size,len(train_x))]
            batch_y = train_y[batch*batch_size:min((batch+1)*batch_size,len(train_y))]    
            opt = sess.run(optimizer, feed_dict={x: batch_x,
                                                              y: batch_y})
            loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x,
                                                              y: batch_y})
        print("Iter " + str(i) + ", Loss= " + \
                      "{:.6f}".format(loss) + ", Training Accuracy= " + \
                      "{:.5f}".format(acc))
        print("Optimization Finished!")
        train_loss.append(loss)
        train_accuracy.append(acc)

    # Calculate accuracy for all 10000 mnist test images
    test_acc,valid_loss = sess.run([accuracy,cost], feed_dict={x: test_x,y : test_y})
    test_loss.append(valid_loss)
    test_accuracy.append(test_acc*100)
    print("Testing Accuracy:","{:.5f}".format(test_acc))
    conv1, conv2, conv3, conv4, out, weights = sess.run([conv1, conv2, conv3, conv4, out, weights], feed_dict={x: train_x})

# Save the weight matrix before normalization
io.savemat('cnn_weights/bn_wc1', {'wc1': weights['wc1']})
io.savemat('cnn_weightsbn_wc2', {'wc2': weights['wc2']})
io.savemat('cnn_weights/bn_wd1', {'wd1': weights['wd1']})
io.savemat('cnn_weights/out', {'out': weights['out']})

# Date based Normalization
def data_norm(activation,weight,previous_factor):
    max_wt = 0
    max_act = 0  
    max_wt = np.max(weight)
    max_act = np.max(activation)
    scale_factor = max(max_wt,max_act)
    applied_factor = scale_factor / previous_factor
    weight = np.divide(weight,applied_factor)
    previous_factor = scale_factor
    return weight, previous_factor

previous_factor = 1
weights['wc1'],previous_factor = data_norm(conv1,weights['wc1'],previous_factor)
weights['wc2'],previous_factor = data_norm(conv3,weights['wc2'],previous_factor)
weights['wd1'],previous_factor = data_norm(conv4,weights['wd1'],previous_factor)

# Save the weight matrix after normalization
io.savemat('cnn_weights/an_wc1', {'wc1': weights['wc1']})
io.savemat('cnn_weights/an_wc2', {'wc2': weights['wc2']})
io.savemat('cnn_weights/an_wd1', {'wd1': weights['wd1']})   