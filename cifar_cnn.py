import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import hdf5storage as io
from keras.utils import to_categorical

# Loading the CIFAR-10 Dataset
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
test_x = np.divide(x_test[:],255.)
test_y = to_categorical(y_test)

# Constants
training_iters = 50 
learning_rate = 1 
batch_size = 50
n_input = 32
n_classes = 10

# Place Holders
x = tf.placeholder("float", [None, 32,32,3])
y = tf.placeholder("float", [None, n_classes])

# CL and PL Definition
def conv2d(x, W, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    return tf.nn.relu(x) 

def maxpool2d(x, k=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='SAME')

# Loading the weight matrix
weights = {}
weights['wc1'] = io.loadmat('cifar_weights/wc1.mat')['wc1']
weights['wc2'] = io.loadmat('cifar_weights/wc2.mat')['wc2']
weights['wc3'] = io.loadmat('cifar_weights/wc3.mat')['wc3']
weights['wd1'] = io.loadmat('cifar_weights/wd1.mat')['wd1']
weights['out'] = io.loadmat('cifar_weights/out.mat')['out']

# Building the Network
conv1 = conv2d(x, weights['wc1'])
conv2 = maxpool2d(conv1, k=2)
conv3 = conv2d(conv2, weights['wc2'])
conv4 = maxpool2d(conv3, k=2)
conv5 = conv2d(conv4, weights['wc3'])
conv6 = maxpool2d(conv5, k=2)
	
# Fully connected layer
fc1 = tf.reshape(conv6, [-1, 4096])
fc1 = tf.matmul(fc1, weights['wd1'])
fc1 = tf.nn.relu(fc1)
out = tf.matmul(fc1, weights['out'])

# Evaluation of Accuracy
correct_prediction = tf.equal(tf.argmax(out, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    test_acc, out = sess.run([accuracy, out], feed_dict={x: test_x,y : test_y})
    print("Testing Accuracy:","{:.5f}".format(test_acc*100))